FROM registry.cern.ch/ml/kf-14-tensorflow:v4

USER root

COPY gans.py /home/jovyan/gans.py
COPY mnist.py /home/jovyan/mnist.py

COPY requirements.txt /requirements.txt
RUN apt-get -qq update && pip3 install -r /requirements.txt

RUN chown -R jovyan:users /home/jovyan/

USER jovyan
WORKDIR /home/jovyan
