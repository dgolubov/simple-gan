import tensorflow_datasets as tfds
import numpy as np
import tensorflow as tf
from tensorflow.keras import layers
import argparse
import time

cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True, 
                                                   reduction=tf.keras.losses.Reduction.SUM)

def normalize_img(image, label_category):
  image = tf.cast(image, tf.float32) # TFDS provide the images as tf.uint8, while the model expect tf.float32, so normalize images
  image = tf.image.resize(image, (DOWNSAMPLE_IMAGE_WIDTH, DOWNSAMPLE_IMAGE_HEIGHT))
  image = (image - 127.5) / 127.5 # Normalize the images to [-1, 1]
  return image, label_category


def make_generator_model():
    model = tf.keras.Sequential()
    # project
    model.add(layers.Dense(24*24*256, use_bias=False, input_shape=(100,)))
    model.add(layers.BatchNormalization())
    model.add(layers.LeakyReLU(alpha=0.2))
    # reshape
    model.add(layers.Reshape((24, 24, 256)))
    assert model.output_shape == (None, 24, 24, 256) # Note: None is the batch size
    # upsample or deconv1
    model.add(layers.Conv2DTranspose(128, (5, 5), strides=(1, 1), padding='same', use_bias=False))
    assert model.output_shape == (None, 24, 24, 128)
    model.add(layers.BatchNormalization())
    model.add(layers.LeakyReLU(alpha=0.2))
    # upsample or deconv2
    model.add(layers.Conv2DTranspose(64, (5, 5), strides=(2, 2), padding='same', use_bias=False))
    assert model.output_shape == (None, 48, 48, 64)
    model.add(layers.BatchNormalization())
    model.add(layers.LeakyReLU(alpha=0.2))
    # upsample or deconv3
    model.add(layers.Conv2DTranspose(1, (5, 5), strides=(2, 2), padding='same', use_bias=False, activation='tanh'))
    assert model.output_shape == (None, 96, 96, 1)
    return model


def make_discriminator_model():
    model = tf.keras.Sequential()
    # conv 1
    model.add(layers.Conv2D(64, (5, 5), strides=(2, 2), padding='same',
                                     input_shape=[96, 96, 1]))
    model.add(layers.LeakyReLU(alpha=0.2))
    model.add(layers.Dropout(0.4))
    # conv 2
    model.add(layers.Conv2D(128, (5, 5), strides=(2, 2), padding='same'))
    model.add(layers.LeakyReLU(alpha=0.2))
    model.add(layers.Dropout(0.4))

    model.add(layers.Flatten())
    model.add(layers.Dense(1))
    return model


def discriminator_loss(real_output, fake_output):
    # compares the discriminator's predictions on real images to an array of 1s (class 1 - real)
    real_loss = cross_entropy(tf.ones_like(real_output), real_output)
    # compares the discriminator's predictions on fake (generated) images to an array of 0s (class 0 - fake)
    fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
    total_loss = real_loss + fake_loss
    return total_loss


def generator_loss(fake_output):
    # if the generator is performing well, the discriminator will classify the fake images as real (or 1)
    # therefore, compare the discriminators decisions on the generated images to an array of 1s
    return cross_entropy(tf.ones_like(fake_output), fake_output)


@tf.function
def train_step(images):
    # Generate noise(or seed) drawn from standard normal distribution 
    noise = tf.random.normal([BATCH_SIZE, noise_dim])
    
    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
      # Pass this noise(or seed) as an input to generator
      # This noise(or seed) is used to produce an image
      generated_images = generator(noise, training=True)
      # Classify real images using discriminator
      # Note that the output is a probability value
      real_output = discriminator(images, training=True)
      # Classify fake images using discriminator
      fake_output = discriminator(generated_images, training=True)
      # Calculate the loss
      gen_loss = generator_loss(fake_output)
      disc_loss = discriminator_loss(real_output, fake_output)
      
    # Calculate the gradients (or partial derivatives)
    gradients_of_generator = gen_tape.gradient(gen_loss, generator.trainable_variables)
    gradients_of_discriminator = disc_tape.gradient(disc_loss, discriminator.trainable_variables)
    
    # Apply the gradients (backpropagation)
    generator_optimizer.apply_gradients(zip(gradients_of_generator, generator.trainable_variables))
    discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, discriminator.trainable_variables))

    return gen_loss, disc_loss


def train(dataset, epochs):
  gen_loss_list = []
  disc_loss_list = []
  
  for epoch in range(epochs):
    start = time.time()
    print(epoch)
    for i, image_batch in enumerate(dataset):
      print('batch', str(i))  
      gen_loss, disc_loss = train_step(image_batch[0])
      print('gen_loss', str(gen_loss))
      print('disc_loss', str(disc_loss))
      
    gen_loss_list.append(gen_loss.numpy())
    disc_loss_list.append(disc_loss.numpy())

    print('gen_loss_list', str(gen_loss_list))
    print('disc_loss_list', str(disc_loss_list))

    print('time for one epoch:', str(time.time() - start))
  
  return gen_loss_list, disc_loss_list


parser = argparse.ArgumentParser()
parser.add_argument("--epochs", default='20')
parser.add_argument("--datadir", default='/eos/user/d/dgolubov/tf_datasets')
parser.add_argument("--dataset", default='smallnorb')
args = parser.parse_args()

(ds_train, ds_test), ds_info = tfds.load(
    args.dataset,
    split=['train', 'test'],
    as_supervised=True,
    with_info=True,
    data_dir=args.datadir,
    download=False)

DOWNSAMPLE_IMAGE_WIDTH = 96
DOWNSAMPLE_IMAGE_HEIGHT = 96
BUFFER_SIZE = 24300 # Size of total dataset
BATCH_SIZE = 1024

ds_train_nrm = ds_train.map(
    normalize_img, num_parallel_calls=tf.data.experimental.AUTOTUNE)
# Take only label_category 4 (cars) for image generation if required, also change BUFFER_SIZE accordingly
# ds_train_nrm = ds_train_nrm.filter(lambda x,y: tf.reduce_all(tf.not_equal(y, [0,1,2,3])))
ds_train_nrm = ds_train_nrm.cache()
ds_train_nrm = ds_train_nrm.shuffle(ds_train_nrm.reduce(0, lambda x, _: x + 1).numpy())  # shuffle(length_of_dataset)
ds_train_nrm = ds_train_nrm.batch(BATCH_SIZE)
ds_train_nrm = ds_train_nrm.prefetch(tf.data.experimental.AUTOTUNE)

generator = make_generator_model()
discriminator = make_discriminator_model()

# Two optimizers to train two networks separately
generator_optimizer = tf.keras.optimizers.Adam(learning_rate=0.0002, beta_1=0.5)
discriminator_optimizer = tf.keras.optimizers.Adam(learning_rate=0.0002, beta_1=0.5)

noise_dim = 100
num_examples_to_generate = 25
seed = tf.random.normal([num_examples_to_generate, noise_dim])

strategy = tf.distribute.MultiWorkerMirroredStrategy()
with strategy.scope():
    gen_loss_list, disc_loss_list = train(ds_train_nrm, int(args.epochs))
    
print('done')
